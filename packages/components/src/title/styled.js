import styled from 'styled-components';

export default styled.h1`
font-family: "system-ui", sans-serif;
font-weight: normal;
text-align: center;
font-size: 2rem;
color: ${props => props.theme.color};
`
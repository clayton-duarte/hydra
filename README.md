
# Hydra

Managing monorepo ease

### Commands

```
lerna bootstrap - installs and links everything
```

```
lerna run dev - builds and serves all applications
```

```
lerna add [module] - installs new module
```

```
lerna exec [command] - executes [command] in every application
```


### Usage

run

```
lerna run dev
```

This will: 

- starts React-App on port 3000
- starts Angular-App on port 4000
- starts Next-App on port 5000
- starts Storybook on port 6006

Go to /packages/component/src and try to change some stuff!

See the magic happens on all places (except on Angular-App for now).


### Structure

- root/
- - packages/
- - - angular-app/package.json
- - - components/package.json
- - - next-app/package.json
- - - react-app/package.json
- package.json
- lerna.json

